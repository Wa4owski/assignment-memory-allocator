#define _DEFAULT_SOURCE
#include <unistd.h>
#include "mem_internals.h"
#include "mem.h"
#include "util.h"

#define INITIAL_SIZE 4196


void* heap_address;
struct block_header* block_h;
//extern inline block_size size_from_capacity( block_capacity cap );

void test1() {
    printf("Test 1:\n");
    int* test = _malloc(1000);
    debug_heap(stdout, block_h);
    if (block_h -> is_free != false || block_h -> capacity.bytes != 1000) {
        printf("Test 1 failed.\n");
        return;
    }
    debug_heap(stdout, heap_address);
    _free(test);
    printf("Test 1 passed.\n");
}

void test2() {
    printf("Test 2:\n");

    void *t1 = _malloc(100);
    void *t2 = _malloc(100);
    if (t1 == NULL || t2 == NULL) {
        printf("Test 2 failed. Can not allocate.\n");
    }

    debug_heap(stdout, block_h);
    _free(t1);
    debug_heap(stdout, block_h);

    struct block_header *t1_block = block_get_header(t1);
    struct block_header *t2_block = block_get_header(t2);

    if (t1_block -> is_free == false || t2_block -> is_free == true) {
        printf("Test 2 failed.\n");
        return;
    }
    _free(t2);
    printf("Test 2 passed.\n");
}

void test3() {
    printf("Test 3:\n");
    void* t1 = _malloc(200);
    void* t2 = _malloc(100);
    debug_heap(stdout, block_h);

    _free(t1);

    struct block_header* h1 = block_get_header(t1);
    struct block_header* h2 = block_get_header(t2);

    if (!h1 -> is_free || h2 -> is_free) {
        printf("Test 3 failed. Allocation and freeing failed. \n");
    	return;
    }
    debug_heap(stdout, block_h);
    _free(t2);
    printf("Test 3 passed.\n");
}

void test4() {
    printf("Test 4:\n");

    void* t1 = _malloc(5000);
    void* t2 = _malloc(4000);
    void* t3 = _malloc(500);

    debug_heap(stdout, block_h);
    _free(t1);
    _free(t2);
    debug_heap(stdout, block_h);

    struct block_header *t1_block = block_get_header(t1);
    struct block_header *t2_block = block_get_header(t2);
    struct block_header *t3_block = block_get_header(t3);

    if (t1_block -> is_free == false || t2_block -> is_free == false || t3_block -> is_free == true) {
        printf("Test 4 failed.\n");
        return;
    }

    _free(t3);
    printf("Test 4 passed.\n");

}

void test5() {
    printf("Test 5:\n");
    struct block_header* last_block = block_h;
    while(last_block->next){
	last_block = last_block->next;
    }

    mmap( (void*) ( (uint8_t*) last_block + size_from_capacity(last_block->capacity).bytes), 1000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED , 0, 0 );
    debug_heap(stdout, block_h);
    void* data = _malloc(5 * 10000);
    debug_heap(stdout, block_h);
    struct block_header* bl_h = block_get_header(data);
    if ((void*)last_block == (void*)(bl_h->contents+bl_h->capacity.bytes)) {
        printf("Test 5 failed. \n");
        return;
    }
    _free(data);
    printf("Test 5 passed.\n");
}


int main() {
    heap_address = heap_init(INITIAL_SIZE);
    if (heap_address == NULL) {
        printf("Initialization failed.\n");
        return -1;
    }

    printf("Successful initialization.\n Begin testing...");
    block_h = (struct block_header*) heap_address;
    test1();
    test2();
    test3();
    test4();
    test5();
    return 0;
}
